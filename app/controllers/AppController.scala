package controllers

import play.api.mvc._
import views.html
import core.{SecureSocial, Identity, Authorization}

object AppController extends Controller with core.SecureSocial with userLoggedIn {

  def index =
      Action{implicit request =>
        Ok(views.html.index(user))
      }

}

trait userLoggedIn{
  implicit def user(implicit request: RequestHeader):Option[Identity] = {
    SecureSocial.currentUser
  }


//// An Authorization implementation that only authorizes uses that logged in using twitter
//case class WithProvider(provider: String) extends Authorization {
//  def isAuthorized(user: Identity) = {
//    user.id.providerId == provider
//  }
}