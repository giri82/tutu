package models

class Generes {

  val list = List(
    "Drama",
    "Thriller",
    "Action",
    "Romantic",
    "Comedy",
    "Sci fi"
  )

}
